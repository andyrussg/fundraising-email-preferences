<?php

namespace Mediawiki\Extension\FundraisingEmailPreferences;

class SpecialEmailPreferencesCenter extends \UnlistedSpecialPage {
	public function __construct() {
		parent::__construct( 'EmailPreferencesCenter' );
	}

	public function execute( $par ) {
		$this->setHeaders();
		$output = $this->getOutput();

		// Remove normal WikiMedia screen elements (header, footer, sidebar, etc.)
		// FIXME Prevent unwanted elements from flashing on the screen before
		// disappearing
		$output->addModules( 'ext.fundraisingEmailPreferences' );
	}

	public function getDescription() {
		return $this->msg( 'fundraisingemailpreferences-page-title' )->text();
	}
}